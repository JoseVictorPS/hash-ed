#include <stdlib.h>
#include <stdio.h>

typedef struct aluno{
  int mat;
  float cr;
  int prox,ant;
}TA;

#define TAM 101

typedef TA *Hash[TAM];

void inicializa(Hash tab, int N){
  int i;
  for(i = 0; i < N; i++) tab[i] = NULL;
}

TA* aloca(int mat, float cr){
  TA* novo = (TA*) malloc(sizeof (TA));
  novo->mat = mat;
  novo->cr = cr;
  novo->prox = -1;
  novo->ant = -1;
  return novo;
}

int hash(int mat, int N){
  return mat % N;
}

int tent_linear(int mat, int N, int k){
  return (hash(mat, N) + k) % N;
}

void libera(Hash tab, int N){
  int i;
  for (i = 0; i < N; i++) if(tab[i]) free(tab[i]);
}

int acha_inicio_cadeia(Hash tab, int N, int pos){
  while(tab[pos]->ant != -1) pos = tab[pos]->ant;
  return pos;
}

int prim_pos_hash(Hash tab, int N, int mat){
  int i = 0, h, pos_livre, achou = 0;
  while(1){
    h = tent_linear (mat, N, i++);
    if(tab[h]){
      if(hash(tab[h]->mat, N) == hash(mat, N))
        return acha_inicio_cadeia(tab, N, h);
    }
    else if(!achou){
      pos_livre = h;
      achou = 1;
    }
    if(i == N)return pos_livre;
  }
}

int busca(Hash tab, int N, int mat){
  int pos = prim_pos_hash(tab, N, mat);
  if(!tab[pos])return -1;
  TA *resp = tab[pos];
  while(resp->mat != mat){
    if(resp->prox == -1)return -1;
    pos = resp->prox;
    resp = tab[pos];
  }
  return pos;
}

void insere(Hash tab, int N, int mat, float cr){
  int ini = prim_pos_hash(tab,N,mat);
  int ant = -1, prox;
  if(tab[ini]){
    prox = ini;
    while(prox != -1){
      if(tab[prox]->mat == mat){
        tab[prox]->cr = cr;
        return;
      }
      ant = prox;
      prox = tab[prox]->prox;
    }
  }
  int i=0, h;
  h = tent_linear(mat, N, i++);
  while(tab[h]) h = tent_linear(mat, N, i++);
  tab[h] = aloca(mat,cr);
  if(ant != -1){
    tab[ant]->prox = h;
    tab[h]->ant = ant;
  }
}

float retira(Hash tab, int N, int mat){
  int ind = busca(tab, N, mat);
  if(ind == -1) return -1;
  TA *p = tab[ind];
  float cr = p->cr;
  if(p->ant == -1){
    if(p->prox != -1)tab[p->prox]->ant = -1;
    free(p);
    tab[ind] = NULL;
    return cr;
  }
  if(p->prox == -1){
    if(p->ant != -1)tab[p->ant]->prox = -1;
    free(p);
    tab[ind] = NULL;
    return cr;
  }
  int ant = p->ant, prox = p->prox;
  tab[prox]->ant = ant;
  tab[ant]->prox = prox;
  free(p);
  tab[ind] = NULL;
  return cr;
}

void imprime(Hash tab, int n){
  int i, h;
  for(i = 0; i < n; i++){
    h = prim_pos_hash(tab, n, i);
    if(tab[h]){
      printf("%d:\n", h);
      TA *p = tab[h];
      while(1){
        printf("\tmat: %d\tcr: %f\tant: %d\tprox: %d\n", p->mat, p->cr, p->ant, p->prox);
        if(p->prox == -1) break;
        p = tab[p->prox];
      }
    }
  }
}

int main(void){
  int n, m;
  printf("Digite m (tamanho da tabela hash)... ");
  scanf("%d", &m);
  printf("Digite n (quantidade de matriculas)... ");
  scanf("%d", &n);

  if((n <= 1) || (m <= 1) || (m < (3 * n / 2))) return 0;

  Hash tab;
  inicializa(tab, m);

  int i, mat;
  float cr;
  for(i = 0; i < n; i++){
    printf("Digite a matricula e o cr...\n");
    scanf("%d", &mat);
    scanf("%f", &cr);
    insere(tab, m, mat, cr);
  }
  imprime(tab, m);

  char resp;
  do{
    printf("Digite a matricula a ser removida... ");
    scanf("%d", &mat);
    cr = retira(tab, m, mat);
    if(cr != -1) printf("%d\t%f saiu\n", mat, cr);
    printf("Quer continuar? ");
    scanf(" %c", &resp);
  }while((resp != 'N') && (resp != 'n'));
  imprime(tab, m);

  do{
    printf("Digite a matricula a ser procurada... ");
    scanf("%d", &mat);
    int ind = busca(tab, m, mat);
    if(ind == -1) printf("Elemento nao encontrado!\n");
    else{
      TA *p = tab[ind];
      printf("%d\t%f\n", p->mat, p->cr);
    }
    printf("Quer continuar? ");
    scanf(" %c", &resp);
  }while((resp != 'N') && (resp != 'n'));
  imprime(tab, m);

  for(i = 0; i < n/2; i++){
    printf("Digite a matricula e o cr...\n");
    scanf("%d", &mat);
    scanf("%f", &cr);
    insere(tab, m, mat, cr);
  }
  imprime(tab, m);

  libera(tab, m);

  return 0;
}
