#include <stdio.h>
#include <stdlib.h>

typedef struct aluno{
  int mat, prox, status;
  float cr;
}TA;

int hash(int mat, int tam){
  return mat % tam;
}

void inicializa(char *arq, int n){
  FILE *fp = fopen(arq, "wb");
  if(!fp) exit(1);
  int i, elem = -1;
  for(i = 0; i < n; i++)fwrite(&elem, sizeof(int), 1, fp);
  fclose(fp);
}

TA* aloca(int mat, float cr){
  TA *novo = (TA*)malloc(sizeof(TA));
  novo->mat = mat;
  novo->cr = cr;
  novo->prox = -1;
  novo->status = 1;
  return novo;
}

TA* busca(char *tabHash, char *dados, int n, int mat){
  FILE *fp = fopen(tabHash,"rb");
  if(!fp)exit(1);
  int pos, h = hash(mat,n);
  fseek(fp, h*sizeof(int), SEEK_SET);
  fread(&pos, sizeof(int), 1, fp);
  fclose(fp);
  if(pos == -1)return NULL;
  fp = fopen(dados,"rb");
  if(!fp) exit(1);
  fseek(fp, pos, SEEK_SET);
  TA aux;
  fread(&aux, sizeof(TA), 1, fp);
  while(1){
    if((aux.mat == mat) && (aux.status)){
      TA *resp = aloca(aux.mat, aux.cr);
      resp->prox = aux.prox;
      fclose(fp);
      return resp;
    }
    if(aux.prox == -1){
      fclose(fp);
      return NULL;
    }
    fseek(fp, aux.prox, SEEK_SET);
    fread(&aux, sizeof(TA), 1, fp);
  }
}

float retira(char *tabHash, char *arq, int n, int mat){
  FILE *fp = fopen(tabHash,"rb");
  if(!fp) exit(1);
  int pos, h = hash(mat,n);
  fseek(fp, h*sizeof(int), SEEK_SET);
  fread(&pos, sizeof(int), 1, fp);
  fclose(fp);
  if(pos == -1)return -1;
  float cr = -1;
  fp = fopen(arq,"rb+");
  if(!fp)exit(1);
  TA aux;
  while(1){
    fseek(fp, pos, SEEK_SET);
    fread(&aux, sizeof(TA), 1, fp);
    if((aux.mat == mat) && (aux.status)){
      cr = aux.cr;
      aux.status = 0;
      fseek(fp, pos, SEEK_SET);
      fwrite(&aux, sizeof(TA), 1, fp);
      fclose(fp);
      return cr;
    }
    if(aux.prox == -1){
      fclose(fp);
      return cr;
    }
    pos = aux.prox;
  }
}

void insere(char *tabHash, char *arq, int n, int mat, float cr){
  FILE *fph = fopen(tabHash, "rb+");
  if(!fph) exit(1);
  int pos, h = hash(mat, n);
  fseek(fph, h*sizeof(int), SEEK_SET);
  fread(&pos, sizeof(int), 1, fph);
  int ant, prim_pos_livre;
  ant = prim_pos_livre = -1;
  FILE *fp = fopen(arq,"rb+");
  if(!fp){
    fclose(fph);
    exit(1);
  }
  TA aux;
  while(pos != -1){
    fseek(fp, pos, SEEK_SET);
    fread(&aux, sizeof(TA), 1, fp);
    if(aux.mat == mat){
      aux.cr = cr;
      aux.status = 1;
      fseek(fp, pos, SEEK_SET);
      fwrite(&aux, sizeof(TA), 1, fp);
      fclose(fp);
      fclose(fph);
      return;
    }
    if((!aux.status) && (prim_pos_livre == -1))prim_pos_livre=pos;
    ant = pos;
    pos = aux.prox;
  }
  if(prim_pos_livre == -1){
    aux.mat = mat;
    aux.cr = cr;
    aux.prox = -1;
    aux.status = 1;
    fseek(fp, 0L, SEEK_END);
    pos = ftell(fp);
    fwrite(&aux, sizeof(TA), 1, fp);
    if(ant != -1){
      fseek(fp, ant, SEEK_SET);
      fread(&aux, sizeof(TA), 1, fp);
      aux.prox = pos;
      fseek(fp, ant, SEEK_SET);
      fwrite(&aux, sizeof(TA), 1, fp);
    }
    else{
      fseek(fph, h*sizeof(int), SEEK_SET);
      fwrite(&pos, sizeof(int), 1, fph);
    }
    fclose(fp);
    fclose(fph);
    return;
  }
  fseek(fp, prim_pos_livre, SEEK_SET);
  fread(&aux, sizeof(TA), 1, fp);
  aux.mat = mat;
  aux.cr = cr;
  aux.status = 1;
  fseek(fp, prim_pos_livre, SEEK_SET);
  fwrite(&aux, sizeof(TA), 1, fp);
  fclose(fp);
  fclose(fph);
  return;
}

void imprime (char *nome_hash, char *nome_dados, int m){
  FILE *fp = fopen(nome_hash, "rb");
  if(!fp) exit(1);
  int vet[m];
  fread(&vet, sizeof(int), m, fp);
  fclose(fp);

  fp = fopen(nome_dados, "rb");
  if(!fp) exit(1);
  int i, pos;
  for(i = 0; i < m; i++){
    int p = vet[i];
    if(p != -1) printf("%d:\n", i);
    TA x;
    while(p != -1){
      fseek(fp, p, SEEK_SET);
      pos = ftell (fp);
      fread(&x, sizeof(TA), 1, fp);
      printf("$%d: matricula: %d\tcr: %f\tstatus: %d\tprox_end: $%d\n", pos, x.mat, x.cr, x.status, x.prox);
      p = x.prox;
    }
  }
  fclose(fp);
}


void limpeza(char *hashf, char *dados, char *novodados, int N) {
  //Abre os arquivos
  FILE * dados_file = fopen(dados, "rb+");
  if(!dados_file) exit(1);

  FILE * novos = fopen(novodados, "wb");
  if(!novos) exit(1);

  FILE * hash_file = fopen(hashf, "rb+");
  if(!hash_file) exit(1);

  //Iterar pelo arquivo de dados e primeiro e ultimo aluno ativo e proximo aluno da iteracao
  TA aluno_iterator, primeiro_aluno, proximo_aluno, ultimo_aluno;

  //Cursor atual da iteração, e cursor do final do arquivo
  int atual = ftell(hash_file);
  fseek(hash_file, 0L, SEEK_END);
  int fim = ftell(hash_file);
  rewind(hash_file);

  //Auxiliar para arquivo de dados, auxiliares para amarraros proximos, e ultimo aluno ativo
  int pos_em_dados, amarrar, pos_iterator, pos_ultimo;
  while(atual < fim) {
    //Lê posição para arquivo de dados do arquivo hash
    atual = ftell(hash_file);
    fread(&pos_em_dados, sizeof(int), 1, hash_file);
    

    //Se há dados com o hash que estamos iterando
    if(pos_em_dados != -1) {
      //Achando o primeiro aluno válido
      fseek(dados_file, pos_em_dados, SEEK_SET);
      fread(&primeiro_aluno, sizeof(TA), 1, dados_file);

      //Busca primeiro aluno ativo
      if(!primeiro_aluno.status) {
        while(!primeiro_aluno.status && primeiro_aluno.prox != -1) {
          fseek(dados_file, primeiro_aluno.prox, SEEK_SET);
          fread(&primeiro_aluno, sizeof(TA), 1, dados_file);
        }

        //Se nenhum aluno com o hash atual estiver ativo
        if(!primeiro_aluno.status) {
          amarrar = -1;
          fseek(hash_file, atual, SEEK_SET);
          fwrite(&amarrar, sizeof(int), 1, hash_file);
          continue;
        }

      }
      
      //Escreve o primeiro aluno ativo no novo arquivo
      amarrar = ftell(novos);
      fwrite(&primeiro_aluno, sizeof(TA), 1, novos);

      //Atualiza o primeiro aluno válido em hash file
      fseek(hash_file, atual, SEEK_SET);
      fwrite(&amarrar, sizeof(int), 1, hash_file);

      //Atribui variaveis considerando que o primeiro aluno é válido
      pos_iterator = amarrar;
      pos_ultimo = amarrar;
      ultimo_aluno = primeiro_aluno;
      aluno_iterator = primeiro_aluno;
      //Insere o próximo aluno no arquivo
      while(aluno_iterator.prox != -1) {

        //Lê o próximo aluno
        fseek(dados_file, aluno_iterator.prox, SEEK_SET);
        fread(&proximo_aluno, sizeof(TA), 1, dados_file);

        //Se ele for válido
        if(proximo_aluno.status) {
          amarrar = ftell(novos);
          fwrite(&proximo_aluno, sizeof(TA), 1, novos);

          //Amarra seu endereço ao .prox do último aluno válido
          ultimo_aluno.prox = amarrar;
          fseek(novos, pos_ultimo, SEEK_SET);
          fwrite(&ultimo_aluno, sizeof(TA), 1, novos);
          
        }

        //Se o próximo aluno for válido, atualiza o último válido
        if(proximo_aluno.status) {
          pos_ultimo = aluno_iterator.prox;
          ultimo_aluno = proximo_aluno;
        }

        //Atualiza o aluno da iteração para o próximo
        pos_iterator = aluno_iterator.prox;
        aluno_iterator = proximo_aluno;
      }

    }

  }

  fclose(dados_file);
  fclose(novos);
  fclose(hash_file);
}

void limpezaReal(char *hashf, char *dados, char *novodados, int N) {
  limpeza(hashf, dados, novodados, N);

  FILE * novos = fopen(novodados, "rb");
  if(!novos) exit(1);
  //Apaga todo o conteúdo do arquivo de dados
  FILE * dados_file = fopen(dados, "wb");
  if(!dados_file) exit(1);

  //Sobreescreve o arquivo de dados com o que tinha em "novodados"
  TA aluno_iterator;
  while(fread(&aluno_iterator, sizeof(TA), 1, novos) == 1) {
    fwrite(&aluno_iterator, sizeof(TA), 1, dados_file);
  }

  //Remove o arquivo auxiliar "novodados"
  remove(novodados);

  fclose(dados_file);
}


void ffretira(char *tabHash, char *dados, int n, int mat, float cr){
  FILE *fp = fopen(tabHash,"rb");
  if(!fp) exit(1);

  int pos, h = hash(mat,n);
  fseek(fp, h*sizeof(int), SEEK_SET);
  fread(&pos, sizeof(int), 1, fp);

  fclose(fp);

  if(pos == -1)return;

  fp = fopen(dados,"rb+");
  if(!fp)exit(1);
  TA aux;

  while(1){
    fseek(fp, pos, SEEK_SET);
    fread(&aux, sizeof(TA), 1, fp);

    if((aux.cr <= cr) && (aux.status)){
      aux.status = 0;
      fseek(fp, pos, SEEK_SET);
      fwrite(&aux, sizeof(TA), 1, fp);
    }

    if(aux.prox == -1){
      fclose(fp);
      return;
    }
    pos = aux.prox;
  }
}


int main(void){
  //m é o tamanho do Hash, e n é a qtd de elementos que vai inserir
  int n, m;
  printf("Digite m... ");
  scanf("%d", &m);
  printf("Digite n... ");
  scanf("%d", &n);

  if((n <= 1) || (m <= 1)) return 0;

  char nome_dados[31], nome_hash[31];
  printf("Digite nome do arquivo de dados... ");
  scanf("%s", nome_dados);
  printf("Digite nome do arquivo de hash... ");
  scanf("%s", nome_hash);

  inicializa(nome_hash, m);
  FILE *fp = fopen(nome_dados, "wb");
  if(!fp) exit(1);
  fclose(fp);

  int i, mat;
  float cr;
  for(i = 0; i < n; i++){
    printf("Digite a matricula e o cr...\n");
    scanf("%d", &mat);
    scanf("%f", &cr);
    insere(nome_hash, nome_dados, m, mat, cr);
  }
  imprime(nome_hash, nome_dados, m);

  char resp;
  do{
    printf("Digite a matricula a ser removida... ");
    scanf("%d", &mat);
    cr = retira(nome_hash, nome_dados, m, mat);
    if(cr != -1) printf("%d %f saiu\n", mat, cr);
    printf("Quer continuar? ");
    scanf(" %c", &resp);
  }while((resp != 'N') && (resp != 'n'));
  imprime(nome_hash, nome_dados, m);

  /*printf("Digite a matricula a ser procurada... ");
  scanf("%d", &mat);
  TA *r = busca(nome_hash, nome_dados, m, mat);
  if(!r)
    printf("Elemento nao encontrado!\n");
  else{
    printf("matricula: %d\tstatus: %d\tprox_end: %d\n", r->mat, r->status, r->prox);
    free(r);
  }*/
  
  /*do{
    printf("Digite a matricula e o cr... ");
    scanf("%d", &mat);
    scanf("%f", &cr);
    insere(nome_hash, nome_dados, m, mat, cr);
    printf("Quer continuar? ");
    scanf(" %c", &resp);
  }while((resp != 'N') && (resp != 'n'));
  imprime(nome_hash, nome_dados, m);*/

  printf("Digite uma matricula e cr para operar a QUESTAO 2: ");
  scanf("%d%f", &mat, &cr);

  printf("\nAPOS A RETIRADA DOS DA QUESTAO 2:\n");
  ffretira(nome_hash, nome_dados, m, mat, cr);
  imprime(nome_hash, nome_dados, m);

  printf("\nAPOS LIMPEZA:\n");
  limpezaReal(nome_hash, nome_dados, "novos.dat", m);

  imprime(nome_hash, nome_dados, m);

  return 0;
}

